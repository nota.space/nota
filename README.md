# nota
Hier ein paar grundlegende Infos zur Tübingen-Version von nota und zur technischen Produktionsorganisation.

## Zeitplan/Milestones
GitLab Anmerkung: Welche Issues welchen Milestone haben, kann in der Liste gesehen werden oder man kann das Board nach Milestones filtern.
### 01.11.18 Dekstop-Montage-Version
Diese Version muss es dem Rest vom MCD möglich machen, mit der Montage von Mediendateien zu beginnen.
### 01.12.18 Tübingen-Version
Endversion, die am 05.12.18 zur Eröffnung des Tübinger Zimmertheaters auf dem Server des Zimmertheaters eingerichtet sein muss.

## GitLab-Projektstruktur
### nota
GitLab Projekt für den Gesamtüberblick, für Test-Feedback, Issues können sowohl backend als auch frontend betreffen
### nota-backend
GitLabProjekt für backend-Quellcode, technische Aufschlüsselung der Issues aus dem GitLab Projekt "nota"
### nota-frontend
GitLabProjekt für frontend-Quellcode, technische Aufschlüsselung der Issues aus dem GitLab Projekt "nota"

## Bedienung/Tastenkürzel
```
Drag                        --  Bewegen
Scroll down                 --  Herauszoomen
Scroll up                   --  Heranzoomen
Shift+DragFragment          --  Verschieben
Shift+DragFragmentBorder    --  Beschneiden/Größe ändern
Strg+Shift+Links            --  Rückgängig
Strg+Shift+Rechts           --  Wiederholen
Strg+Shift+Drag             --  Auswahlrahmen
```
